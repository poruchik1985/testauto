<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 18.01.2017
 * Time: 21:25
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Autopart;
use AppBundle\Form\AutopartType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class AdminController extends Controller {
    /**
     * @Route("/admin", name="admin")
     */
    public function indexAction()
    {
        return $this->render('admin/index.html.twig');
    }

    public function manufacturersAction(){
        return $this->render('admin/manufacturers.html.twig');
    }

}