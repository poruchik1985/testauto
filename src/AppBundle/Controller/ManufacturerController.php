<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21.01.2017
 * Time: 20:33
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Manufacturer;
use AppBundle\Form\ManufacturerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ManufacturerController  extends Controller {
    /**
     * @Route("/admin/manufacturers", name="manufacturers")
     */
    public function manufacturersAction()
    {
        $manufacturers = $this->getDoctrine()->getRepository("AppBundle:Manufacturer")->findBy(array(), array('id' => 'ASC'));
        $form = $this->createForm(ManufacturerType::class, new Manufacturer(), array(
            'action' => $this->generateUrl('manufacturer_save'),
        ));
        return $this->render(
            'admin/manufacturers.html.twig', array(
                'manufacturers' => $manufacturers,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/admin/manufacturers/save", name="manufacturer_save")
     */
    public function manufacturerSaveAction(Request $request){

        if($request && $request->request->get('updated-item-id')){
            $manufacturer = $this->getDoctrine()->getRepository('AppBundle:Manufacturer')->find($request->request->get('updated-item-id'));
        }
        else{
            $manufacturer = new Manufacturer();
        }
        $form = $this->createForm(ManufacturerType::class, $manufacturer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($manufacturer);
            $em->flush();
        }
        return $this->redirectToRoute('manufacturers');
    }

    /**
     * @Route("/admin/manufacturers/delete/{id}", name="manufacturer_delete")
     */
    public function manufacturerDeleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $manufacturer = $em->getRepository("AppBundle:Manufacturer")->find($id);
        $em->remove($manufacturer);
        $em->flush();
        return $this->redirectToRoute('manufacturers');
    }

}