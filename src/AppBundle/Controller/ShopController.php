<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 22.01.2017
 * Time: 0:36
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ShopController extends Controller {
    /**
     * @Route("/shop", name="shop")
     */
    public function indexAction(){
        $products = $this->getUser()->getProducts()->toArray();
        $orders = $this->searchOrders();
        usort($products, function($product1, $product2){
            return $product1->getId() - $product2->getId();
        });
        $form = $this->createForm(ProductType::class, new Product(), array(
            'action' => $this->generateUrl('product_save'),
        ));

        return $this->render('shop/index.html.twig', array(
            'products' => $products,
            'form' => $form->createView(),
            'orders' => $orders,
        ));
    }

    /**
     * @Route("/shop/save", name="product_save")
     */
    public function productSaveAction(Request $request){
        $product = new Product();


        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $product->setIsPresent(true);
            $product->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
        }
        return $this->redirectToRoute('shop');
    }

    /**
     * @Route("/shop/update", name="product_update")
     */
    public function productUpdateAction(Request $request){
        $data = $request->request->get('data');

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository("AppBundle:Product")->find($data['id']);
        $product->setIsPresent($data['isPresent']);
        $em->persist($product);
        $em->flush();
        return new Response();
    }

    private function searchOrders(){
        $repository = $this->getDoctrine()->getRepository('AppBundle:Order');
        $resultOrders = array();
        $products = $this->getUser()->getPresentedProducts()->toArray();
        foreach($products as $product){
            $resultOrders = array_merge($resultOrders, ($repository->createQueryBuilder('o')
                ->where('o.maxCost >= :cost')
                ->andWhere('o.minCost <= :cost')
                ->andWhere('o.autopart = :autopart')
                ->andWhere('o.manufacturer = :manufacturer')
                ->andWhere('o.isActive = true')
                ->setParameter('cost', $product->getCost())
                ->setParameter('autopart', $product->getAutopart())
                ->setParameter('manufacturer', $product->getManufacturer())
                ->getQuery()->getResult())
        );
        }
        return $resultOrders;
    }
}