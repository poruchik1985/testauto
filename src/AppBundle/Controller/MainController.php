<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 18.01.2017
 * Time: 21:25
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Order;
use AppBundle\Form\OrderType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class MainController extends Controller {
    /**
     * @Route("/", name="mainpage")
     */
    public function indexAction(){
        if(!$this->getUser()){
            return $this->redirectToRoute('login');
        }
        if($this->get('security.authorization_checker')->isGranted('ROLE_SHOP')){
            return $this->redirectToRoute('shop');
        }

        $orders = $this->getUser()->getOrders()->toArray();
        usort($orders, function($order1, $order2){
            return $order1->getId() - $order2->getId();
        });
        $form = $this->createForm(OrderType::class, new Order(), array(
            'action' => $this->generateUrl('order_save'),
        ));
        return $this->render('index.html.twig', array(
            'orders' => $orders,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/test", name="test")
     */
    public function testAction()
    {
        return $this->render('test.html.twig');
    }

    /**
     * @Route("/save", name="order_save")
     */
    public function orderSaveAction(Request $request){
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order->setUser($this->getUser());
            $order->setIsActive(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
        }
        $shopEmails = $this->searchProducts($order);
        foreach ($shopEmails as $shopEmail){
            $msg = array('address' => $shopEmail);
            $this->get('old_sound_rabbit_mq.message_sender_producer')->setContentType('application/json');
            $this->get('old_sound_rabbit_mq.message_sender_producer')->publish(serialize($msg));
        }
        return $this->redirectToRoute('mainpage');
    }

    /**
     * @Route("/update", name="order_update")
     */
    public function orderUpdateAction(Request $request)
    {
        $data = $request->request->get('data');
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository("AppBundle:Order")->find($data['id']);
        $order->setIsActive(false);
        $em->persist($order);
        $em->flush();

        return new Response();
    }

    private function searchProducts(Order $order)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');

        $products = $repository->createQueryBuilder('p')
                ->where('p.cost <= :maxCost')
                ->andWhere('p.cost >= :minCost')
                ->andWhere('p.autopart = :autopart')
                ->andWhere('p.manufacturer = :manufacturer')
                ->andWhere('p.isPresent = true')
                ->setParameter('minCost', $order->getMinCost())
                ->setParameter('maxCost', $order->getMaxCost())
                ->setParameter('autopart', $order->getAutopart())
                ->setParameter('manufacturer', $order->getManufacturer())
                ->getQuery()->getResult();
        $shopEmails = array();
        foreach ($products as $product){
            $shopEmails[] = $product->getUser()->getEmail();
        }
        $shopEmails = array_unique($shopEmails);
        return $shopEmails;
    }
}