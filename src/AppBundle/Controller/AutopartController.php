<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21.01.2017
 * Time: 20:33
 */

namespace AppBundle\Controller;



use AppBundle\Entity\Autopart;
use AppBundle\Form\AutopartType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AutopartController  extends Controller {
    /**
     * @Route("/admin/autoparts", name="autoparts")
     */
    public function autopartsAction(){
        $autoparts = $this->getDoctrine()->getRepository("AppBundle:Autopart")->findBy(array(), array('id' => 'ASC'));
        $form = $this->createForm(AutopartType::class, new Autopart(), array(
            'action' => $this->generateUrl('autopart_save'),
        ));
        return $this->render(
            'admin/autoparts.html.twig', array(
                'autoparts' => $autoparts,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/admin/autoparts/save", name="autopart_save")
     */
    public function autopartSaveAction(Request $request){
        if($request && $request->request->get('updated-item-id')){
            $autopart = $this->getDoctrine()->getRepository('AppBundle:Autopart')->find($request->request->get('updated-item-id'));
        }
        else{
            $autopart = new Autopart();
        }

        $form = $this->createForm(AutopartType::class, $autopart);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($autopart);
            $em->flush();
        }
        return $this->redirectToRoute('autoparts');
    }

    /**
     * @Route("/admin/autoparts/delete/{id}", name="autopart_delete")
     */
    public function autopartDeleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $autopart = $em->getRepository("AppBundle:Autopart")->find($id);
        $em->remove($autopart);
        $em->flush();
        return $this->redirectToRoute('autoparts');
    }

}