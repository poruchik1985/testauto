<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21.01.2017
 * Time: 16:06
 */

namespace AppBundle\Entity;
use AppBundle\Entity\Autopart;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="app_order")
 * @ORM\Entity
 */
class Order {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $minCost;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxCost;

    /**
     * @ORM\ManyToOne(targetEntity="Autopart", inversedBy="orders")
     * @ORM\JoinColumn(name="autopart_id", referencedColumnName="id")
     */
    private $autopart;

    /**
     * @ORM\ManyToOne(targetEntity="Manufacturer", inversedBy="orders")
     * @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
     */
    private $manufacturer;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(name="is_active", type="boolean", options={"default":true})
     */
    private $isActive;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set autopart
     *
     * @param Autopart $autopart
     *
     * @return Order
     */
    public function setAutopart(Autopart $autopart = null)
    {
        $this->autopart = $autopart;

        return $this;
    }

    /**
     * Get autopart
     *
     * @return Autopart
     */
    public function getAutopart()
    {
        return $this->autopart;
    }

    /**
     * Set manufacturer
     *
     * @param \AppBundle\Entity\Manufacturer $manufacturer
     *
     * @return Order
     */
    public function setManufacturer(\AppBundle\Entity\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return \AppBundle\Entity\Manufacturer
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set minCost
     *
     * @param integer $minCost
     *
     * @return Order
     */
    public function setMinCost($minCost)
    {
        $this->minCost = $minCost;

        return $this;
    }

    /**
     * Get minCost
     *
     * @return integer
     */
    public function getMinCost()
    {
        return $this->minCost;
    }

    /**
     * Set maxCost
     *
     * @param integer $maxCost
     *
     * @return Order
     */
    public function setMaxCost($maxCost)
    {
        $this->maxCost = $maxCost;

        return $this;
    }

    /**
     * Get maxCost
     *
     * @return integer
     */
    public function getMaxCost()
    {
        return $this->maxCost;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Order
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Order
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
