<?php
namespace AppBundle\Form;

use AppBundle\Entity\Order;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('autopart', EntityType::class, array(
                'class' => 'AppBundle:Autopart',
                'choice_label' => 'name',
                'placeholder' => 'Выберите запчасть'
            ))->add('manufacturer', EntityType::class, array(
                'class' => 'AppBundle:Manufacturer',
                'choice_label' => 'name',
                'placeholder' => 'Выберите производителя'
            ))->add('minCost', IntegerType::class, array(
                    'label' => 'Цена от',
            ))->add('maxCost', IntegerType::class, array(
                'label' => 'Цена до',
            ));
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => Order::class,
        ));
    }
}