<?php
namespace AppBundle\Form;

use AppBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('autopart', EntityType::class, array(
                'class' => 'AppBundle:Autopart',
                'choice_label' => 'name',
                'placeholder' => 'Выберите запчасть'
            ))->add('manufacturer', EntityType::class, array(
                'class' => 'AppBundle:Manufacturer',
                'choice_label' => 'name',
                'placeholder' => 'Выберите производителя'
            ))->add('cost', IntegerType::class, array(
                    'label' => 'Цена',
            ));
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => Product::class,
        ));
    }
}