<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 22.01.2017
 * Time: 22:16
 */

namespace AppBundle;


use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class Mailer implements ConsumerInterface {
    public function execute(AMQPMessage $msg){
        $address = unserialize($msg->body)['address'];
        $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo($address)
            ->setBody(
                'Появилась новая заявка, подходящая под ваш продукт',
                'text/html'
            );

        $spool = new \Swift_FileSpool("/var/www/testauto/app/emails/");
        $transport = \Swift_SpoolTransport::newInstance($spool);
        $mailer = new \Swift_Mailer($transport);
        $mailer->send($message);
    }
}