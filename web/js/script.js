/**
 * Created by Admin on 21.01.2017.
 */
$(function(){

    $('select').material_select();
    $('.modal').modal();
    $(".order-updater").click(function () {
        var id = $(this).parent().find('.order-updater-id').val();
        var url = Routing.generate('order_update');
        $.post(url, {data:{'id':id}});
        $(this).hide();
        $(this).parent().find('.order-closed').show();
    });
});
