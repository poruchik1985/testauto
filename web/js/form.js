/**
 * Created by Admin on 20.01.2017.
 */
$(function(){
    setFieldsVisible('#user_isShop');
    $('#user_isShop').click(function () {
        setFieldsVisible(this);
    });
});

function setFieldsVisible(isShopChecker){
    if($(isShopChecker).prop('checked')){
        $('.person-field').hide();
        $('.shop-field').show();
    }
    else{
        $('.person-field').show();
        $('.shop-field').hide();
    }
}