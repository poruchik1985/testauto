/**
 * Created by Admin on 21.01.2017.
 */
$(function(){

    $('select').material_select();
    $('.modal').modal();
    $(".product-updater").click(function () {
        var isPresent = Number($(this).prop('checked'));
        var id = $(this).parent().find('.product-updater-id').val();
        var url = Routing.generate('product_update');

        $.post(url, {data:{'id':id, 'isPresent':isPresent}});
    });
    /*$(".autopart-editor").click(function () {
        var id = $(this).parent().find(".autopart-id").val();
        var name = $(this).parent().find(".autopart-name").html();

        $('#updated-item-id').val(id);
        $('#autopart_name').focus();
        $('#autopart_name').val(name);
        $('#update-reset-button').show();
        $('#update-button').html('Сохранить');
        $('#update-title').html('Редактирование');
    });
*/

});
