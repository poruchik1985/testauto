/**
 * Created by Admin on 21.01.2017.
 */
$(function(){
    $('#update-reset-button').hide();

    $(".autopart-editor").click(function () {
        var id = $(this).parent().find(".autopart-id").val();
        var name = $(this).parent().find(".autopart-name").html();

        $('#updated-item-id').val(id);
        $('#autopart_name').focus();
        $('#autopart_name').val(name);
        $('#update-reset-button').show();
        $('#update-button').html('Сохранить');
        $('#update-title').html('Редактирование');
    });

    $(".manufacturer-editor").click(function () {
        var id = $(this).parent().find(".manufacturer-id").val();
        var name = $(this).parent().find(".manufacturer-name").html();

        $('#updated-item-id').val(id);
        $('#manufacturer_name').focus();
        $('#manufacturer_name').val(name);
        $('#update-reset-button').show();
        $('#update-button').html('Сохранить');
        $('#update-title').html('Редактирование');
    });
    $("#update-reset-button").click(function () {
        $('#manufacturer_name').focusout();
        $('#updated-item-id').val('');
        $('#manufacturer_name').val('');
        $('#update-reset-button').hide();
        $('#update-button').html('Добавить');
        $('#update-title').html('Добавить производителя');
    });
});
